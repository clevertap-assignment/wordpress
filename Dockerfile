# FROM wordpress:latest


# # RUN rm -rf /var/www/html/*



# # COPY ["custom-plugins","/usr/src/wordpress/wp-content/plugins"]

# # Updating the configuration of wordpress image with our own
# COPY ./config/uploads.ini /usr/local/etc/php/conf.d/uploads.ini

# # Applying the execution right on the folders for apache
# COPY entrypoint-child.sh /usr/bin/
# RUN chmod +x /usr/bin/entrypoint-child.sh
# # ENTRYPOINT ["entrypoint-child.sh"]
# CMD ["apache2-foreground"]

#########
FROM wordpress:latest


# RUN rm -rf /var/www/html/*
# COPY . /var/www/html
COPY ["customplugins","/usr/src/wordpress/wp-content/plugins"]
COPY ["customplugins", "/var/www/html/wp-content/plugins"]
COPY ["my-new-plugin", "/var/www/html"]
COPY ["my-new-plugin", "/usr/src/wordpress"]

COPY entrypoint-child.sh /usr/bin/
# RUN chmod +x /usr/bin/entrypoint-child.sh
# ENTRYPOINT ["entrypoint-child.sh"]


CMD ["apache2-foreground"]

COPY ./config/uploads.ini /usr/local/etc/php/conf.d/uploads.ini