<?php
   /*
   Plugin Name: Awesomeness Creator
   Plugin URI: https://my-awesomeness-emporium.com
   description: >-
  a plugin to create awesomeness and spread joy
   Version: 1.2
   Author: Mr. Awesome
   Author URI: https://mrtotallyawesome.com
   License: GPL2
   */

   function wpb_follow_us($content) {
 
      // Only do this when a single post is displayed
      if ( is_single() ) { 
       
      // Message you want to display after the post
      // Add URLs to your own Twitter and Facebook profiles
       
      $content .= '<p class="follow-us">Text from super plugin</p>';
       
      } 
      // Return the content
      return $content; 
       
      }
      // Hook our function to WordPress the_content filter
      add_filter('the_content', 'wpb_follow_us'); 
